<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Date                                   _f9fd10</name>
   <tag></tag>
   <elementGuidId>74adab19-ed15-462b-8282-fb0a07247d65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row-fluid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

              
              
                Date
                
                  
                    Start
                    
                      
                      Please select a date / time first
                    
                  
                  
                    End
                    
                      
                      Please select a date / time first
                    
                  
                
                
                
                  
                    Start
                    
                      
                      Please select a date first
                    
                  
                  
                    End
                    
                      
                      Please select a date first
                    
                  
                
                Business Hours
                
                  
                    Start
                    
                      
                      Please select a time first
                    
                  
                  
                    End
                    
                      
                      Please select a time first
                    
                  
                
                Business Days
                
                  Sun
                  Mon
                  Tue
                  Wed
                  Thr
                  Fri
                  Sat
                
                
                
                
                  
                    Start
                    
                      
                      Please select a date first
                    
                  
                  
                    End
                    
                      
                      Please select a date first
                    
                  
                
                Business Days
                
                  
                    Sun
                    
                    
                  
                  
                    Mon
                    
                    
                  
                  
                    Tue
                    
                    
                  
                  
                    Wed
                    
                    
                  
                  
                    Thr
                    
                    
                  
                  
                    Fri
                    
                    
                  
                  
                    Sat
                    
                    
                  
                
                
                Mower Options
                
                  
                    Mower width
                    
                      
                      Metres
                    
                  
                  
                    Mower on input
                    
                      
                    
                  
                
                The time limit of this request is 90 days.
                Advance option ▼
                
                  
                    Minimum Time
                    
                      
                      Seconds
                    
                  
                  
                    Petrol radius
                    
                      
                      m
                    
                  
                  
                    Minimum Time
                    
                      
                      Minutes
                    
                    Show map preview (max 3 days period)
                  
                  
                    Show graph
                  
                  
                    Speeding: Use report id
                  
                  
                    Mask Address
                  
                  Stationary Option
                  
                    
                      Max Speed
                      
                        
                        km/h
                        
                      
                    
                    
                      Max Distance
                      
                        
                        m
                        
                      
                    
                  
                  
                    
                      Min Time
                      
                        
                        second(s)
                        
                      
                    
                  
                  
                    
                      Use Historical Geofences
                      * Please note report processing may take longer
                    
                  
                  
                    Filter Report ID
                    
                      
                        
                      
                    
                  
                  
                    Minimum Extended Engine Off Time
                    
                      
                      Minutes
                    
                    Minimum Extended Engine Idle Time
                    
                      
                      Minutes
                    
                    Road Speeding Limit
                    
                      
                      %
                    
                  
                  
                    Minimum Extended Engine Off Time
                    
                      
                      Minutes
                    
                    Minimum Extended Engine Idle Time
                    
                      
                      Minutes
                    
                    
                    Minimum Speeding Time
                    
                      
                      seconds
                    
                    Speed Exceed Minimum
                    
                      
                      km/h
                    
                    
                    Minimum Seatbelt OFF Duration
                    
                      
                      seconds
                    
                    Minimum Seatbelt OFF Speed
                    
                      
                      km/h
                    
                    
                    Minimum 2WD Duration
                    
                      
                      minutes
                    
                    Minimum 2WD Speed
                    
                      
                      km/h
                    
                  
                  
                    Show journey
                    Minimum Trip Duration
                    
                      
                      Minutes
                    
                    Minimum Trip Distance
                    
                      
                      km
                    
                    Exceptions
                    Check for Speeding
                    Speed limit
                    
                      
                      km/h
                    
                    Report ID
                    
                      
                        
                      
                    
                  
                  Hide option ▲
                
                
                  Inbox
                  Outbox
                

              
              

              
              
                Device(s)
                  
                    All
                    By device group
                  
                
                
                  A52 (Pekanbaru) (2019110034)A53 (Pekanbaru) (2019110035)A54 (Pekanbaru) (2019110036)A55 (Pekanbaru) (2019110037)A56 (Pekanbaru) (2019110038)A57 (Pekanbaru) (2019110039)A58 (Pekanbaru) (2019110040)A59 (Pekanbaru) (2019110041)A60 (Pekanbaru) (2019110042)A61 (Pekanbaru) (2019110043)Aruman Device 2 (20202020)Aruman Device 3 (12121212)Aruman Device 4 (23232323)Aruman Device Test (222992291)Aruman Testing (1234567890)Avanza Esky (20129270)B 8099 SE (Innova IT-360) (2111111172)B11 (GT06E_avanza) (80616337)Berkah (2003300547)Camera Avanza - JC100 (80120093)Camera Innova - JC100 (80124368)Demo Avanza (2000330001)Demo Avanza (IT-30) (2003301408)Demo Camera (2003360072)Demo Innova (2000300001)Demo personal (1000300005)deviceBaru (222266666)ES-610 Danny (20939891)ES-610 Guntur (20926666)ES-610 Robi (20950179)ES310_AVANZA_HAHB (2063100294)es310_inova_HAHB (2063100293)ES310_TEST2 (6462872)ES610 (avanza_HA_HB) (30449180)es610_inova (HA_HB) (31829893)ex Teknisi Berkah (30449586)ex Teknisi Prasetyo (2000102540)ex Teknisi Riduan (2000123457)ex Teknisi Surya (20931971)ex Teknisi Tama (2000102049)ex Teknisi Wawan (2000102542)F 7932 AA (Trial Ikin Mandiri) Camera (2090370008)G 36 (2019110044)G 37 (2019110045)G 38 (2019110046)G 39 (2019110047)G 40 (2019110048)G 41 (2019110049)G 42 (2019110050)G 43 (2019110051)G 44 (2019110052)G 45 (2019110053)GT06E Test 1 (80075708)GT06E-Test Aaron (84949358)GT06E-Test Geoffrey (84963375)GV-20 0356 (80450356)GV-20 4134 (inova) (80464134)GV-40 Test (90235397)H 92 (test IT-102G_2) (2000102154)HDVR Test 2 (999999991)IT-10 Ahmad (2000100316)IT10 Agung (2000102539)JC100 China Test (80097010)JC100-JKT Test (80100319)jc100_inova_newfw (90172892)JC100_new_fw (90009458)johky test fw1 (2000101958)M34 ex alat surabaya (80616261)Motor Johky (IT10) (2000101617)New Test (200033333)Taiwan Test 01 (2111111113)Taiwan Test 02 (2000100005)Teknisi Agung (2019110008)Teknisi Agus (20934041)Teknisi Amir (2003300428)Teknisi Berkah (2003300698)Teknisi Firman (30470061)Teknisi Hilda (2003300581)Teknisi Imam (2003301148)Teknisi Ramot (30422476)test_BBPPT_EMC_VT360_2 (2000024005)test_GT06E (80818354)Vario Kantor (IT10) (2000102818)X14 (2000103081)X15 (2000103082)X22 (2000103089)X23 (2000103090)Demo Innova (2000300001)
                  Please select at least one device
                
                Driver(s)
                
                  Andri F.Anto EXArumanDarusDybalaFitraIrfanjayamix1jayamix2Jehan RamadhanJohky ChengKunkunMas ImamPuyolRahmatRiduanSofyanTest DriverUbaidillahWemiYudhaSelect driver
                  Please select at least one driver
                
                Polling type
                
                  Check all
                  Position Tracking
                  Engine On
                  Engine Off
                  Connect
                  Disconnect
                  Sync
                
                
                  Geofence(s)
                  Company
                  Intimap Saja
                  Group -
                    Select all
                  
                  aepAncolArumanAruman2COBACustomerdaradedeGAikhaIntimapJehanJohkyMBToffice test 3Rungkut Megah Rayasales surabayaSofyanSteviesugengsurabaya 5Surabaya Testsurabaya test 3surabaya test 4TerminalTes 1Tes 2Tes ArumanTestTest Buzzertestgrup1VirsaWPCyohanes
                  
                    Check all
                  
                  
                    
                    Please select at least one geofence
                  
                
                Exception List
                
                  
                  Extended Idle
                  Speeding
                  Seatbelt OFF
                  4WD Disengaged
                  Main Battery Power Lost
                  Excessive Idle End 
                  Excessive Idle Time
                  Harsh braking
                  Emergency stopping (impact) 
                  Harsh Acceleration 
                  Alarm Triggered
                  SOS Alert. Button Pressed
                  Rollover sensor activated
                  Fatigue management duty time interval reached
                
              
              

              
              
                Action
                Generate
                
                  Download
                  
                    
                  
                  
                    Spreadsheet
                    PDF
                  
                
                
                  Daily Summary
                  Monthly Summary
                
                
                
                  
                
              
              

            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row-fluid&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Polling'])[2]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit position'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]</value>
   </webElementXpaths>
</WebElementEntity>
