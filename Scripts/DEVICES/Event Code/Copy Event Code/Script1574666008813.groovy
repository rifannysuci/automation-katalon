import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Map/a_Devices'))

WebUI.scrollToElement(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Map/a_Event Code'), 3)

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Map/a_Event Code'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/buttton_Copy Event Code'))

WebUI.scrollToElement(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/span_Select a device'), 3)

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/span_Select a device'))

WebUI.setText(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/input'), 
    'CAMER')

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/li_Camera Innova - JC100 (80124368)'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/button_Select device'))

WebUI.setText(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/input_1'), 
    'CAMERA')

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/input_Berkah (2003300547)_multiselect_copy-_bea256'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/button_Copy'))

WebUI.click(findTestObject('Object Repository/DEVICES/EVENT CODE/COPY EVENT CODE/Page_Online IntelliTrac  Event Code/a_OK'))

WebUI.closeBrowser()

