import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Map/a_Devices'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  Map/a_User Marker'))

WebUI.setText(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/input_Add new marker_add-name'), 
    'TEST USER MARKER')

WebUI.selectOptionByValue(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/select_ Choose field                       _d5df2a'), 
    'driver_name', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/select_Choose operator                     _ac75d4'), 
    'between', true)

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/img_Add new marker_add-image'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/img'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/button_Add'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/a_OK'))

WebUI.click(findTestObject('Object Repository/DEVICES/User Device Marker/Page_Online IntelliTrac  User Device Marker/button_Reset'))

WebUI.closeBrowser()

