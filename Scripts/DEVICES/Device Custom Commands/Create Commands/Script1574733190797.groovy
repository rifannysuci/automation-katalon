import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Map/a_Devices'))

WebUI.scrollToElement(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Map/a_Custom Command'), 3)

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Map/a_Custom Command'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/a_New Test'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/button_Create'))

WebUI.selectOptionByValue(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/select_---PASS - PasswordTAGID - Unique Tag Id'), 
    'PASS', true)

WebUI.setText(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/div_PASSTEST COMMAND'), 
    '[PASS]TEST COMMAND')

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/button_Save'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/a_OK'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/div_PASSTEST COMMAND_sp-preview'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/span_GV-40 Test (90235397)_sp-thumb-inner'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/button_choose'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/button_Save'))

WebUI.click(findTestObject('DEVICES/Custom Command/create delete command/Page_Online IntelliTrac  Device Custom Command/a_OK'))

