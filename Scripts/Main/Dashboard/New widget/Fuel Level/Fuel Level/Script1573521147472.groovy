import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Load Device/input_Default group_checkall'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Load Device/a_Load Selected (63)'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Map/a_Dashboard'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/small_New Widget'))

WebUI.scrollToElement(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/a_Fuel Level'), 3)

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/a_Fuel Level'))

WebUI.setText(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/input_Widget Name_widget_name'), 
    'FUEL LEVEL')

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/div_'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/span_Logout_sp-thumb-el sp-thumb-dark'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/button_choose'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/span_Select device'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/li_Aruman Testing'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/span_Select fuel data'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/li_Fuel Realtime (cm)'))

WebUI.click(findTestObject('Object Repository/MAIN/Dashboard/New Widget/Fuel Level/Page_Online IntelliTrac  Dashboard/button_Save changes'))

WebUI.closeBrowser()

