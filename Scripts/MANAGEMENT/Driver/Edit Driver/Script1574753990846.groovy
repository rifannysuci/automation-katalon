import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Map/a_Management'))

WebUI.scrollToElement(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Map/a_Drivers'), 3)

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Map/a_Drivers'))

WebUI.selectOptionByValue(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Drivers/select_20                          50      _fe7095'), 
    '100', true)

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Drivers/a_-_table-icon icomoon-icon-pencil-2'))

WebUI.setText(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Drivers/input_Name_name'), 
    'DRIVER NEWw')

WebUI.click(findTestObject('MANAGEMENT/DRIVER/EDIT DRIVER/Page_Online IntelliTrac  Drivers/a_Update'))

WebUI.closeBrowser()

