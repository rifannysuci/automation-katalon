import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Map/a_Management'))

WebUI.scrollToElement(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Map/a_Users'), 3)

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Map/a_Users'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/a_Create new user'))

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Name_name'), 
    'user intimap baru')

WebUI.setEncryptedText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Password_u-password'), 
    'zqvEdKo+GzBzfhI+7ixTTA==')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Address_u-address'), 
    'jalan radjiman')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Address detail_u-suburb'), 
    'jakarta')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Email_u-email'), 
    'user@gmail.com')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/input_Phone_u-mobile'), 
    '0868837333')

WebUI.click(findTestObject('Object Repository/MANAGEMENT/User/create new user/Page_Online IntelliTrac  Users/a_Save'))

WebUI.closeBrowser()

