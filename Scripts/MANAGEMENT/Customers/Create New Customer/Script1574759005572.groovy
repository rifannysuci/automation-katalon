import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Map/a_Management'))

WebUI.scrollToElement(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Map/a_Customers'), 3)

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Map/a_Customers'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/a_Create new customer'))

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_First name_firstname'), 
    'RIFANNY SUCI')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Surname_surename'), 
    'SYAHDA')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Initial_initial'), 
    'FANNY')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Unit street no_unit_street_no'), 
    '120')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Street name_streetname'), 
    'JALAN RADJIMAN')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Suburb_suburb'), 
    'JAKARTA TIMUR')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_State_state'), 
    'DKI JAKARTA')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Post code_postcode'), 
    '13930')

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Date of birth_dob'))

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/a_26'))

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_concat(Mother  s maiden name)_maiden'), 
    'FANNY')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Password_password'), 
    'FANNY123')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Home number_home_ph'), 
    '085781722685')

WebUI.setText(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/input_Mobile number_mobile_ph'), 
    '085781722685')

WebUI.click(findTestObject('Object Repository/MANAGEMENT/CUSTOMER/CREATE NEW CUSTOMER/Page_Online IntelliTrac  Customers/a_Save'))

WebUI.closeBrowser()

