import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Map/a_Fleet System'))

WebUI.scrollToElement(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Map/a_Route Planner BETA'), 3)

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Map/a_Route Planner BETA'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/button_Create'))

WebUI.setText(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/input_Name_plan_name'), 
    'TEST ROUTE')

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/button_Add depot'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/input_Jl Kb Trubun No96A_multiselect_4'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/div_Create New Route Plan      Import from _884976'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/div'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/button_Add delivery'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/input_Jaya Celcon_multiselect_5'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/div_Create New Route Plan      Import from _884976_1'))

WebUI.click(findTestObject('Object Repository/FLEET SYSTEM/CREATE ROUTE PLANNER/Page_Online IntelliTrac  Route Planner/button_Generate'))

WebUI.closeBrowser()

