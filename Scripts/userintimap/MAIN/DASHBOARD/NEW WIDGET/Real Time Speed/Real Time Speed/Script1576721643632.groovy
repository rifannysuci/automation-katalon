import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'userintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Load Device/input'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Load Device/a_Load Selected (1)'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Map/a_Dashboard'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/small_New Widget'))

WebUI.scrollToElement(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/a_Realtime Speed'), 3)

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/a_Realtime Speed'))

WebUI.setText(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/input_Widget Name_widget_name'), 
    'REALTIME')

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/button_Filter by device group'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/input_Uncheck all_multiselect_device-group'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/body_        Notification                Re_94ce95'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/button_Select device'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/input_Uncheck all_multiselect_devices'))

WebUI.click(findTestObject('Object Repository/userintimap/main/DASHBOARD/NEW WIDGET/Real Time Speed/Page_Online IntelliTrac  Dashboard/button_Save changes'))

WebUI.closeBrowser()

