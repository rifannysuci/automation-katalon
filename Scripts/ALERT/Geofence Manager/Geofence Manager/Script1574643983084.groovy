import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Map/a_Alert'))

WebUI.scrollToElement(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Map/a_Geofence Manager'), 3)

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Map/a_Geofence Manager'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/button_Create Geofence'))

WebUI.setText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Rectangle_search'), 
    'JALAN RADJIMAN')

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Rectangle_search'))

WebUI.setText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Rectangle_search'), 
    'JALAN RADJIMAN')

WebUI.sendKeys(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Rectangle_search'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/li_Jl Dr KRT Radjiman Widyodiningrat Kec Ca_578ca8'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/span_Create Geofence_icomoon-icon-radio-unc_f5c117'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/canvas_Terms of Use_ol-unselectable'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/canvas_Terms of Use_ol-unselectable'))

WebUI.setText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Geofence Name_feat-name'), 
    'TEST CIRCLE')

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/div_Convert to polygon_sp-preview-inner'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/span_choose_sp-thumb-inner'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/button_choose'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/button_Save'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/a_OK'))

WebUI.setText(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Check All_device_filter'), 
    'INN')

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/input_Demo Innova_cb-2000300001'))

WebUI.click(findTestObject('Object Repository/ALERT/GEOFENCE MANAGER/CREATE GEOFENCE/Page_Online IntelliTrac  Geofence Manager/button_Assign'))

WebUI.closeBrowser()

