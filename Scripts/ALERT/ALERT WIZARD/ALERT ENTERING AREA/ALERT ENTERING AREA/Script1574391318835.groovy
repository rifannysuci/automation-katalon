import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Map/a_Alert'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Map/a_Alert Wizard'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/img'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/button_Select geofences'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/input_Uncheck all_multiselect_geofence'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/div_CompanyIntimap SajaGeofenceABCDDepan Ka_773050'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/a_Next'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/span_Work Days_icon'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/a_Next'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/button_Device Name - Device ID'))

WebUI.setText(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/input'), 
    'AVAN')

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/input_Demo Avanza  -  (2000330001)_multisel_39bb4b'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/div_var deviceClass  1X1 Series7X8-BT Serie_cac3a2'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/a_Next'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/a_Submit'))

WebUI.click(findTestObject('Object Repository/ALERT/ALERT WIZARD/ALERT ENTERING AREA/Page_Online IntelliTrac  Alert Wizard/a_OK'))

