import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Map/a_Report v2'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Map/a_View'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_Stationary'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/input_Start_date_start'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_14'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/input_End_date_end'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_18'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/body_        Notification                Re_94ce95'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/dl_Time235906HourMinuteSecondMillisecondTim_9c2c68'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/body_        Notification                Re_94ce95_1'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/span_Device Name - Device ID'))

WebUI.setText(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/input'), 'inno')

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/input_Demo Camera  -  (2003360072)_multisel_4edc8b'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/button_Generate'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/button_Download_btn btn-primary dropdown-toggle'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_Spreadsheet'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/button_Download_btn btn-primary dropdown-toggle'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_PDF'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/button_Download_btn btn-primary dropdown-toggle'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/a_CSV'))

WebUI.click(findTestObject('Object Repository/Report V2/STATIONARY/Page_Online IntelliTrac  Reports/button_Reset form'))

WebUI.closeBrowser()

