import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Map/a_Report v2'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Map/a_Data Analyzer'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/span_Select Device'))

WebUI.setText(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/input'), 
    'inn')

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/li_Demo Innova'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/button_Load Position'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/button_Load Position_generateSetup'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/select_Map SpeedAltitudeSatelliteEvent Code_fff9d4'), 
    'aalti', true)

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/input_Widget Content_btn'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/path_Created with Highcharts 607_highcharts_ced3ab'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/div_Download PNG image'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/path_Created with Highcharts 607_highcharts_ced3ab_1'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/div_Download PDF document'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/span_Load Position_cut-icon-reload'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/a_Yes'))

WebUI.click(findTestObject('Object Repository/Report V2/MASTER REPORT/Data Analyzer/Page_Online IntelliTrac  Data Analyzer/a_OK'))

WebUI.closeBrowser()

