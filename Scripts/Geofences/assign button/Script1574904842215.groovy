import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/a_Logout_entypo-icon-map'))

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/h4_Geofence'))

WebUI.selectOptionByValue(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/select_AncolArumanAruman2COBACustomerdarade_a16eea'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/select_ Please select  AEROTRANSArea Rumah _ab92f5'), 
    '2445', true)

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/a_Assign'))

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/input'))

WebUI.click(findTestObject('Object Repository/GEOFENCES/Assign Button/Page_Online IntelliTrac  Map/a_Assign_1'))

WebUI.closeBrowser()

