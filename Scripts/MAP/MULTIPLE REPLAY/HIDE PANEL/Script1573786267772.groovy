import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://gps-track.intellitrac.co.id/')

WebUI.setText(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Login/input_Online IntelliTrac_username'), 
    'superintimap')

WebUI.setEncryptedText(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Login/input_Online IntelliTrac_password'), 
    'aw2pSrstMnKd7g5+KcY1tw==')

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Login/form_Forgot your password'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Login/input_Online IntelliTrac_submit'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Load Device/a_Load All'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Map/a_Map'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Map/a_Multiple Replay'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/input_Start Date_datepicker1'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/a_9'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/input_End Date_datepicker2'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/a_15'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/button_Select device'))

WebUI.setText(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/input'), 
    'INN')

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/input_Demo Camera (2003360072)_multiselect__5c06d1'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/a_Generate'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/a_Min Dist (M)_hide-panel'))

WebUI.click(findTestObject('Object Repository/MAP/MULTIPLE REPLAY/HIDE PANEL/Page_Online IntelliTrac  Multiple Replay/a_Speed  50_show-panel'))

WebUI.closeBrowser()

